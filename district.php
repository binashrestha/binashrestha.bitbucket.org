<!DOCTYPE html>
<html>
<head>
    <?php include('includes/_head.php')?>
    <title>VDC mapping</title>

</head>
<body >
<?php $district = ucfirst($_GET['district']);?>


<div id = "page">
    <div id ="header-content">
        <div id = "header">
            <img src="main-logo.png" width="195">
        </div>
    </div>

    <div id ="content-wrapper">
        <div id = "navigation"><?php include('includes/_navigation.php')?></div>


        <div class ="main-content" id ="main">


            <div class="container" style ="background-color: #3A3A3A;color :#fff;width: auto">
                <div class="row-fluid">
                    <div class="span12">
                        <ul>
                            <li><a href="index.php"><img src="arrow.png"> Home</a></li>

                            <li><img src="arrow.png"> District: <?php echo $district?> > Vulnerabilty mapping</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="clearall"></div>





            <div id="nepalmap" style="width: auto; float: left;">
                <div className="col-md-9">
                    <div id="map">
                    </div>
                </div>
                <div id ="mapTooltip"><div id ="value"></div></div>
            </div>


            <div class="right-panel" style="float: left; width: 150px;">
                <div class = "col-md-3" style="padding-left: 0px;">
                    <div id ="district-image"><img src = "details/dhading.png"></div>
                </div>

                <div class = "col-md-3 charts-wrapper" style="padding-left: 0px;">
                    <div id ="house-damaged" >
                        <?php include('includes/pie_chart.php')?>
                    </div>
                </div>

                <div id = "label" class = "col-md-3">
                    <table>
                        <tr>
                            <th><div id ="circle"></div></th>
                            <td>Oxfam works</td>
                        </tr>
                        <tr>
                            <th><div id ="red-square" class="square"></div></th>
                            <td>Red</td>
                        </tr>
                        <tr>
                            <th><div id ="yellow-square" class="square"></div></th>
                            <td>Yellow</td>
                        </tr>
                        <tr>
                            <th><div id ="green-square" class="square"></div></th>
                            <td>Green</td>
                        </tr>
                    </table>
                </div>
                <div class = "additional-content"></div>

                <div style="clear:both"></div>
            </div>

            <div style="clear:both"></div>

        </div>


    </div>
</div>

<script type="text/javascript" src ="details/Dhading.js"></script>
<script type="text/javascript">

    //Load in GeoJSON data
    d3.json("geojson/districts/<?php echo $district?>.json", function( error , nepal) {


        //Width and height
        var w = 810;
        var h = 800;
        var divNode = d3.select("#main").node();



        var canvas = d3.select("#map")
                .attr("preserveAspectRatio", "xMinYMin meet")
                .append("svg")
                .attr("width", w)
                .attr("height", h)
                .attr("viewBox", "150 0 804 621")
                .classed("svg-content-responsive", true);

        group = canvas.selectAll("g")
                .data(nepal.features)
                .enter()
                .append("g");


        //Define map projection

        var center = d3.geo.centroid(nepal);
        var projection = d3.geo.mercator()
                .scale(42000)
               .center( center);

        //Define path generator
        var geoPath = d3.geo.path().projection(projection);

        // Define linear scale for output
        var color = d3.scale.linear().range(["#EAE7E8","#FF1D34","#FFC80B","#00AC4F"]);

        var legendText = ["Unknown", "Red", "Yellow", "Green" ];

        // Load in my states data!
        d3.csv("csv/<?php echo strtolower($district)?>-color.csv", function(data) {
            color.domain([0, 1, 2, 3]); // setting the range of the input data
        });
        var plotVDCs = group.append("path")
                .attr("d", geoPath)
                .attr("fill", function (d){
                    return colorById[d.id];
                })
                .attr("stroke", "")
                .attr("stroke-width", "1px")
                .attr("district", function(d){
                    return d.properties.VDC;
                })
                .on("mousemove", function (d){
                    var vdcName = d.properties.VDC;
                    var absoluteMousePos = d3.mouse(divNode);
                    d3.select("#mapTooltip")
                            .style("left", absoluteMousePos[0] + 120+ "px")
                            .style("top", absoluteMousePos[1] - 32+ "px")
                            .select("#value")
                            .attr("text-anchor", "middle")
                            .attr("font-size", "14px")
                            .html('<b>'+vdcName+'</b><br><b>Population : </b>'+populationById[d.id]+'<br><b>Households : </b>' + householdById[d.id]);

                    d3.select("#mapTooltip").classed("hidden", false);

//                    d3.select(this).style("fill", "#163C56");
                })
                .on("mouseout", function(d) {
                    d3.select(this)
                            .style("fill", function (d){
                                return colorById[d.id];
                            })
                    d3.select("#mapTooltip").classed("hidden", true);
                })
                .on('click', function(d){
//                    d3.select('additional-content')
//                        .html('')

                    alert('some chart to show');
                });

/*
        var legend = d3.select("#map").append("svg")
            .attr("class", "legend")
            .attr("width", 140)
            .attr("height", 500)
            .selectAll("g")
            .data(color.domain().slice().reverse())
            .enter()
            .append("g")
            .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

        legend.append("rect")
            .attr("width", 18)
            .attr("height", 18)
            .style("fill", color);

        legend.append("text")
            .data(legendText)
            .attr("x", 24)
            .attr("y", 9)
            .attr("dy", ".35em")
            .text(function(d) { return d; });
*/

        // Map the cities I have lived in!
        d3.csv("csv/<?php echo strtolower($district)?>-worked.csv", function(data) {

            canvas.selectAll("circle")
                .data(data)
                .enter()
                .append("circle")
                .attr("class", "logo")
                .attr("cx", function(d) {
                    return projection([d.lon, d.lat])[0];
                })
                .attr("cy", function(d) {
                    return projection([d.lon, d.lat])[1];
                })
                .attr("r", function(d) {
                    return Math.sqrt(d.size) * 4;
                })
                .style("fill", "#FFFFFF")
                .style("opacity", 1)
                .append("image")
                .attr("xlink:href", "logo.png");
        });

    });

    function getContent(vdc) {

    }


</script>
</body>
</html>