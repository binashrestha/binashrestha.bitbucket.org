var p0 = '#EAE7E8';
var p1 = '#FF1D34' ;//red
var p2 = '#FFC80B' ;//yellow
var p3 = '#00AC4F' ;//green

var colorById = [p1,p2,p1,p3,p2,p1,p2,p1,p1,p2,p2,p1,p3,p2,p1,p1,p1,p2,p1,p1,p1,p2,p1,p2,p2,p1,p1,p2,p2,p1,p1,p2,p2,p2,p1,p3,p1,p2,p1,p1,p1,p1,p1,p2,p2,p1,p1, p0, p0];

var populationById=["3,580","13,369","3,660","11801","10,786","3,732","6,349","7,687","8423","4,033","8950","4,762","13884","5,563","4,833","8,789","4,158","7,744","6,998","8,852","5,906","5,010","7,177","4,381","6,006","14180","3,893","6,824","9,203","3,658","3,175","8,067","14,553","20,182","4,125","11,043","5,434","5,650","4,920","7,892","4985","3,306","3,597","5,151","9,838","2,072","5396", "Unknown", "Unknown"];

var householdById=["873","2,795","859","2142","2,145","866","1,514","1,655","1486","998","1511","1,063","2433","1,133","1,109","1,920","770","1,442","1,653","1,921","1,425","1,104","1,566","1,051","1,163","2620","853","1,243","2,167","849","749","1,876","3,184","5,255","920","2,214","1,110","1,140","1,123","1,791","927","847","817","1,098","2,141","464","975", "Unknown", "Unknown"];

var vdcArray=["Aginchok","Baireni","Baseri","Benighat","Bhumesthan","Budhathum","Chainpur","Chhatredeurali","Darkha","Dhola","Dhussa","Dhuwakot","Gajuri","Goganpani","Gumdi","Jeewanpur","Jharlang","Jogimara","Jyamrung","Kalleri","Katunje","Kewalpur","Khalte","Khari","Kiranchok","Kumpur","Lapa","Mahadevsthan","Maidi","Marpak","Mulpani","Nalang","Naubise","Nilkantha","Phulkharka","Pida","Ree Gaun","Salang","Salyankot","Salyantar","Satyadevi","Semjong","Sertung","Tasarpu","Thakre","Tipling","Tripureshwor", "Sangkosh", "Sunaulabzar"];