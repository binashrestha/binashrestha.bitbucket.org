<!DOCTYPE html>
<html>
<head>
    <?php include('includes/_head.php')?>
    <title>Districts In Nepal</title>
</head>
<body >
<div id = "page">
    <div id ="header-content">
        <div id = "header">
            <img src="main-logo.png" width="195">
        </div>
    </div>

    <div id ="content-wrapper">
        <div id = "navigation"><?php include('includes/_navigation.php')?></div>

        <div class ="main-content" id ="main">
            <div id="nepalmap">
                <div className="col-md-9">
                    <div id="map" class = "nepal-map">
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-offset-7">
                        <div id="legend">
                        </div>
                    </div>
                </div>

                <div id ="mapTooltip">District : <div id ="value"></div></div>
            </div>
        </div>
        <div style = "clear: both"></div>
    </div>
</div>
<script type="text/javascript">

    //Load in GeoJSON data
    d3.json("geojson/districts.json", function( error , nepal) {


        //Width and height
        var w = 1000;
        var h = 800;
        var divNode = d3.select("#main").node();

        var canvas = d3.select("#map")
                .attr("preserveAspectRatio", "xMinYMin meet")
                .append("svg")
                .attr("width", w)
                .attr("height", h)
                .attr("viewBox", "0 0 804 621")
                .classed("svg-content-responsive", true);

        group = canvas.selectAll("g")
                .data(nepal.features)
                .enter()
                .append("g");

        //Define map projection
        var projection = d3.geo.mercator()
                .scale(5500)
                .center([84.985593872070313, 28.465876770019531]);

            //Define path generator
            var geoPath = d3.geo.path().projection(projection);


        var plotDistricts = group.append("path")
                .attr("d", geoPath)
                .attr("fill", function (d){
                    return getcolor(d.properties.Zone);
                })
                .attr("stroke", "")
                .attr("stroke-width", "1px")
                .attr("district", function(d){
                    return d.properties.District;
                })
                .on("mousemove", function (d){
                    var districtName = d.properties.District;
                    var absoluteMousePos = d3.mouse(divNode);
                    d3.select("#mapTooltip")
                            .style("left", absoluteMousePos[0] + 155+ "px")
                            .style("top", absoluteMousePos[1] + "px")
                            .select("#value")
                            .attr("text-anchor", "middle")
                            .attr("font-size", "14px")
                            .html(districtName);

                    d3.select("#mapTooltip").classed("hidden", false);

                    d3.select(this)
                            .style("fill", "#163C56");
                })
                .on("mouseout", function(d) {
                    d3.select(this)
                            .style("fill", function (d){
                                return getcolor(d.properties.Zone);
                            })
                    d3.select("#mapTooltip").classed("hidden", true);
                })
                .on('click', function(d){
                    var district = d.properties.District.toLowerCase();
                    window.open("district.php?district="+district, "_self");
                    d3.select("#mapTooltip").classed("hidden", true);
                });



    });

    function getcolor(zone) {

        if(zone == 'Mechi') {
            return '#1f78b4';
        }
        else if(zone == "Koshi") {
            return '#8dd3c7';
        }
        else if(zone == "Sagarmatha") {
            return '#ffffb3';
        }
        else if(zone == "Janakpur") {
            return '#bebada';
        }
        else if(zone == "Bagmati") {
            return '#fb8072';
        }
        else if(zone == "Narayani") {
            return '#80b1d3';
        }else if(zone == "Gandaki") {
            return '#fdb462';
        }
        else if(zone == "Dhaualagiri") {
            return '#b3de69';
        }
        else if(zone == "Lumbini") {
            return '#d9d9d9';
        }
        else if(zone == "Rapti") {
            return '#bc80bd';
        }
        else if(zone == "Bheri") {
            return '#b2df8a';
        }
        else if(zone == "Karnali") {
            return '#ccebc5';
        }else if(zone == "Seti") {
            return '#ffed6f';
        }
        else {
            return '#a6cee3';
        }

    }

</script>
</body>
</html>