<?php $arrDistricts = array('Achham' , 'Arghakhanchi' , 'Baglung' , 'Baitadi' , 'Bajhang',
                    'Bajura' , 'Banke' , 'Bara' , 'Bardiya' , 'Bhaktapur' , 'Bhojpur' , 'Chitwan',
                    'Dadeldhura', 'Dailekh' , 'Dang' , 'Darchula' , 'Dhading' , 'Dhankuta' , 'Dhanusa',
                    'Dolakha' , 'Dolpa' , 'Doti' , 'Gorkha' , 'Gulmi' , 'Humla' , 'Ilam' , 'Jajarkot' ,
                    'Jhapa' , 'Jumla' , 'Kailali' , 'Kalikot' , 'Kanchanpur' , 'Kapilbastu' , 'Kaski',
                    'Kathmandu' , 'Kavrepalanchok' , 'Khotang' , 'Lalitpur' , 'Lamjung' , 'Mahottari',
                    'Makwanpur' , 'Manang' , 'Morang' , 'Mugu' , 'Mustang','Myagdi' , 'Nawalparasi',
                    'Nuwakot' , 'Okhaldhunga' , 'Palpa' , 'Panchthar' , 'Parbat' , 'Parsa' , 'Pyuthan',
                    'Ramechhap' , 'Rasuwa' , 'Rautahat' , 'Rolpa' , 'Rukum' , 'Rupandehi' , 'Salyan',
                    'Sankhuwasabha' , 'Saptari' , 'Sarlahi' , 'Sindhuli' , 'Sindhupalchok' , 'Siraha',
                    'Solukhumbu' ,'Sunsari' , 'Surkhet' , 'Syangja' , 'Tanahu' , 'Taplejung' ,
                    'Terhathum' , 'Udayapur');?>
<h3>Districts</h3>

    <?php foreach($arrDistricts as $dist) {?>
    <a href ="district.php?district=<?php echo $dist?>">
        <span class="btn btn-primary" style ="margin:5px 0px; font-size:14px;padding:6px"><?php echo $dist?></span></a>
    <?php } ?>
