<div id="pieChart"></div>
<script src="js/d3pie.min.js"></script>
<script>
    var pie = new d3pie("pieChart", {
        "header": {
            "title": {
                "text": "Household damaged VDC",
                "color": "#f0eeee",
                "fontSize": 20,
                "font": "open sans"
            },
            "subtitle": {
                "text": "Oxfam working VDCs",
                "color": "#ffffff",
                "fontSize": 10,
                "font": "open sans"
            },
            "titleSubtitlePadding": 9
        },
        "footer": {
            "color": "#999999",
            "fontSize": 10,
            "font": "open sans",
            "location": "bottom-left"
        },
        "size": {
            "canvasHeight": 470,
            "canvasWidth": 270,
            "pieInnerRadius": "5%",
            "pieOuterRadius": "54%"
        },
        "data": {
            "sortOrder": "value-desc",
            "content": [
                {
                    "label": "Gajuri",
                    "value": 1337,
                    "color": "#f30000"
                },
                {
                    "label": "Benighat",
                    "value": 1147,
                    "color": "#0600f3"
                },
                {
                    "label": "Dhussa",
                    "value": 1213,
                    "color": "#00b109"
                },
                {
                    "label": "Tripureshwor",
                    "value": 822,
                    "color": "#14e4b4"
                },
                {
                    "label": "Satyadevi",
                    "value": 902,
                    "color": "#0fe7fb"
                },
                {
                    "label": " Darkha",
                    "value": 0,
                    "color": "#67f200"
                },
                {
                    "label": "Kumpur",
                    "value": 2586,
                    "color": "#ff7e00"
                }
            ]
        },
        "labels": {
            "outer": {
                "pieDistance": 29
            },
            "inner": {
                "hideWhenLessThanPercentage": 1
            },
            "mainLabel": {
                "color": "#eeebeb",
                "fontSize": 11
            },
            "percentage": {
                "color": "#ffffff",
                "decimalPlaces": 0
            },
            "value": {
                "color": "#e9e3e3",
                "fontSize": 11
            },
            "lines": {
                "enabled": true,
                "style": "straight"
            },
            "truncation": {
                "enabled": true
            }
        },
        "tooltips": {
            "enabled": true,
            "type": "placeholder",
            "string": "{label} : {value}"
        },
        "effects": {
            "pullOutSegmentOnClick": {
                "effect": "back",
                "speed": 400,
                "size": 11
            }
        },
        "misc": {
            "colors": {
                "background": "#34272b",
                "segmentStroke": "#271c1c"
            },
            "gradient": {
                "enabled": true,
                "percentage": 100
            }
        }
    });
</script>
